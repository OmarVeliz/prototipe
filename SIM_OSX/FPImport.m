//
//  FPDiOS.m
//  FPDiOS
//
//  Created by Fernando Godinez Alvarado on 7/10/15.
//  Copyright © 2015 Plus Mobile Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPImport.h"
#include <CommonCrypto/CommonDigest.h>
#import <ifaddrs.h>
#import <arpa/inet.h>
#import <net/if.h>
#import <netdb.h>
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if_dl.h>
#import <sys/utsname.h>
#import <Foundation/NSDistributedNotificationCenter.h>
#include <SystemConfiguration/SystemConfiguration.h>

@implementation FPImport
@synthesize locationManager = _locationManager;
@synthesize vtransactionUUID = _vtransactionUUID;
@synthesize vtransactionType = _vtransactionType;
@synthesize vUsername = _vUsername;
@synthesize vClientDateStamp = _vClientDateStamp;
@synthesize vClientTimeStamp =_vClientTimeStamp;
@synthesize vLatitude =_vLatitude;
@synthesize vLongitude = _vLongitude;
@synthesize vHostName = _vHostName;
@synthesize vMACAddress =_vMACAddress;
@synthesize vIPPrivAddress = _vIPPrivAddress;
@synthesize vIPPublAddress = _vIPPublAddress;
@synthesize vIPHostISP =_vIPHostISP;
@synthesize vDeviceBrand = _vDeviceBrand;
@synthesize vDeviceModel = _vDeviceModel;
@synthesize vDeviceLanguage = _vDeviceLanguage;
@synthesize vDeviceOS =_vDeviceOS;
@synthesize vUserPass = _vUserPass;



-(NSString *)getFPD
{
    
    NSMutableString *returnResult;
    
    [_locationManager setDelegate:self];
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    //[_locationManager requestWhenInUseAuthorization];
    [_locationManager startUpdatingLocation];
    
    [_vtransactionUUID setString:[self getTransactionUUID]];
    [_vtransactionType setString:[self getTransactionType]];
   /* [_vUsername setString:[self getUsername]];
    [_vClientDateStamp setString:[self getDateFormatted]];
    [_vClientTimeStamp setString:[self getTimeFormatted]];
    [_vLatitude setString:[self getLatitude]];
    [_vLongitude setString:[self getLongitude]];
    [_vHostName setString:[self getHostName]];
    [_vMACAddress setString:[self getMACAddress]];
    [_vIPPrivAddress setString:[self getJSDDeviceBrand]];
    [_vIPPublAddress setString:[self getPublicIP]];
    [_vIPHostISP setString:[self getJSDDeviceBrand]];
    [_vDeviceBrand setString:[self getJSDDeviceBrand]];
    [_vDeviceModel setString:[self getJSDDeviceBrand]];
    [_vDeviceLanguage setString:[self getJSDDeviceBrand]];
    [_vDeviceOS setString:[self getJSDDeviceBrand]];
    [_vUserPass setString:[self getJSDDeviceBrand]];*/
 
    
    
    
    //NSData *jsonReturn = [NSJSONSerialization dataWithJSONObject:allVarDictionary options:NSJSONWritingPrettyPrinted error:nil];
    
    return returnResult;
    
}



/************  GETTING INFO METHODS  *********************/

-(NSString *)getTransactionUUID
{
    NSString *returnResult;// = [NSString stringWithFormat:@"%@",uuid_generate(1)];
    return returnResult;
}

-(NSString *)getTransactionType
{
    NSString *returnResult;
       return returnResult;
}








/*******************************************************************/
-(NSString *)getStringFillwith:(NSString *)str size:(int)size withFillChar:(NSString *)fillStr
{
    long add = size - [str length];
    if (add > 0) {
        NSString *pad = [[NSString string] stringByPaddingToLength:add withString:fillStr startingAtIndex:0];
        str = [str stringByAppendingString:pad];
        //NSLog(@"string: '%@'", str);
        //NSLog(@"pad: '%@'", pad);
    }
    
    return str;
}
/*******************************************************************/


/************  GETTING INFO METHODS  *********************/


-(NSString *)getHostName
{
    NSString *returnResult = (NSString *)CFBridgingRelease(SCDynamicStoreCopyLocalHostName(NULL));
    return returnResult;
}

-(NSString *)getPublicIP
{
    NSString *returnResult = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://bot.whatismyipaddress.com/"] encoding:NSUTF8StringEncoding error:nil];
    return returnResult;
}


-(NSString *)getMacAddress
{
    int mgmtInfoBase[6];
    char *msgBuffer = NULL;
    NSString *errorFlag = NULL;
    size_t length;
    // Setup the management Information Base (mib)
    mgmtInfoBase[0] = CTL_NET; // Request network subsystem
    mgmtInfoBase[1] = AF_ROUTE; // Routing table info
    mgmtInfoBase[2] = 0;
    mgmtInfoBase[3] = AF_LINK; // Request link layer information
    mgmtInfoBase[4] = NET_RT_IFLIST; // Request all configured interfaces
    // With all configured interfaces requested, get handle index
    if ((mgmtInfoBase[5] = if_nametoindex("en0")) == 0)
        errorFlag = @"if_nametoindex failure";
    // Get the size of the data available (store in len)
    else if (sysctl(mgmtInfoBase, 6, NULL, &length, NULL, 0) < 0)
        errorFlag = @"sysctl mgmtInfoBase failure";
    // Alloc memory based on above call
    else if ((msgBuffer = malloc(length)) == NULL)
        errorFlag = @"buffer allocation failure";
    // Get system information, store in buffer
    else if (sysctl(mgmtInfoBase, 6, msgBuffer, &length, NULL, 0) < 0) {
        free(msgBuffer);
        errorFlag = @"sysctl msgBuffer failure";
    } else {
        // Map msgbuffer to interface message structure
        struct if_msghdr *interfaceMsgStruct = (struct if_msghdr *) msgBuffer;
        // Map to link-level socket structure
        struct sockaddr_dl *socketStruct = (struct sockaddr_dl *) (interfaceMsgStruct + 1);
        // Copy link layer address data in socket structure to an array
        unsigned char macAddress[6];
        memcpy(&macAddress, socketStruct->sdl_data + socketStruct->sdl_nlen, 6);
        // Read from char array into a string object, into traditional Mac address format
        NSString *macAddressString = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                                      macAddress[0], macAddress[1], macAddress[2], macAddress[3], macAddress[4], macAddress[5]];
        //NSLog(@"Mac Address: %@", macAddressString);
        // Release the buffer memory
        free(msgBuffer);
        return macAddressString;
    }
    // Error...
    NSLog(@"Error: %@", errorFlag);
    return nil;
}


-(NSString *)getMacIPAddress
{
    //NSString *returnResult = @"";
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];                }            }            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    //return returnResult;
    /*SCDynamicStoreRef storeRef = SCDynamicStoreCreate(NULL, (CFStringRef)@"FindCurrentInterfaceIpMac", NULL, NULL);
    CFPropertyListRef global = SCDynamicStoreCopyValue (storeRef,CFSTR("State:/Network/Interface"));
    id primaryInterface = [(__bridge NSDictionary *)global valueForKey:@"Interfaces"];
    
    for (NSString* item in primaryInterface)
    {
        NSLog(@"%@", item);
        returnResult = item;
    }*/
    
    //return returnResult;
    
    /*NSArray *ipAddresses = [[NSHost currentHost] addresses];
    NSArray *sortedIPAddresses = [ipAddresses sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.allowsFloats = NO;
    
    for (NSString *potentialIPAddress in sortedIPAddresses)
    {
        if ([potentialIPAddress isEqualToString:@"127.0.0.1"]) {
            continue;
        }
        
        NSArray *ipParts = [potentialIPAddress componentsSeparatedByString:@"."];
        
        BOOL isMatch = YES;
        
        for (NSString *ipPart in ipParts) {
            if (![numberFormatter numberFromString:ipPart]) {
                isMatch = NO;
                break;
            }
        }
        if (isMatch) {
            return potentialIPAddress;
        }
    }
    
    // No IP found
    return @"?.?.?.?";*/
    
    
}

                                                                                                                                                                                                                                                                                                                                                                                         


/******************************************************************************/


/*--------------*/

-(NSString *)getDeviceBrand
{
    NSString *returnResult = @"Apple Inc.";
    return returnResult;
}


-(NSString *)getMacDeviceModel
{
    //NSString *returnResult = [[NSString alloc] init];
   /* struct utsname systemInfo;
    uname(&systemInfo);
    NSString *code = [NSString stringWithUTF8String:systemInfo.machine];
    static NSDictionary *deviceNamesByCode;
    if (!deviceNamesByCode) {
        deviceNamesByCode = @{@"i386"      :@"Simulator",
                              @"iPod1,1"   :@"iPod Touch",      // (Original)
                              @"iPod2,1"   :@"iPod Touch",      // (Second Generation)
                              @"iPod3,1"   :@"iPod Touch",      // (Third Generation)
                              @"iPod4,1"   :@"iPod Touch",      // (Fourth Generation)
                              @"iPhone1,1" :@"iPhone",          // (Original)
                              @"iPhone1,2" :@"iPhone",          // (3G)
                              @"iPhone2,1" :@"iPhone",          // (3GS)
                              @"iPad1,1"   :@"iPad",            // (Original)
                              @"iPad2,1"   :@"iPad 2",          //
                              @"iPad3,1"   :@"iPad",            // (3rd Generation)
                              @"iPhone3,1" :@"iPhone 4",        // (GSM)
                              @"iPhone3,3" :@"iPhone 4",        // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" :@"iPhone 4S",       //
                              @"iPhone5,1" :@"iPhone 5",        // (model A1428, AT&T/Canada)
                              @"iPhone5,2" :@"iPhone 5",        // (model A1429, everything else)
                              @"iPad3,4"   :@"iPad",            // (4th Generation)
                              @"iPad2,5"   :@"iPad Mini",       // (Original)
                              @"iPhone5,3" :@"iPhone 5c",       // (model A1456, A1532 | GSM)
                              @"iPhone5,4" :@"iPhone 5c",       // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" :@"iPhone 5s",       // (model A1433, A1533 | GSM)
                              @"iPhone6,2" :@"iPhone 5s",       // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" :@"iPhone 6 Plus",   //
                              @"iPhone7,2" :@"iPhone 6",        //
                              @"iPad4,1"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   :@"iPad Mini",       // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   :@"iPad Mini"        // (2nd Generation iPad Mini - Cellular)
                              };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
    }
    
    */
    char *model;
    size_t len = 0;
    sysctlbyname("hw.model", NULL, &len, NULL, 0);
    if (len) {
        model = malloc(len*sizeof(char));
        sysctlbyname("hw.model", model, &len, NULL, 0);
        //printf("%s\n", model);
    }
    NSString *returnResult = [NSString stringWithFormat:@"%s", model];
    return returnResult;
}

-(NSString *)getMacLanguage
{
    NSString *returnResult = [[NSLocale preferredLanguages] objectAtIndex:0];
    return returnResult;
}


-(NSString *)getMacDeviceOS
{
    NSString *returnResult;
    if ([[NSProcessInfo processInfo] respondsToSelector:@selector(operatingSystemVersion)]) {
        NSOperatingSystemVersion version = [[NSProcessInfo processInfo] operatingSystemVersion];
        // do whatever you want with the version struct here
        returnResult = [NSString stringWithFormat:@"%li.%li.%li",(long)version.majorVersion,version.minorVersion,version.patchVersion];
    }
    else {
        UInt32 systemVersion = 0;
        //OSStatus err = Gestalt(gestaltSystemVersion, (SInt32 *) &systemVersion);
        // do whatever you want with the systemVersion as before
        returnResult = [NSString stringWithFormat:@"%u",((unsigned int)systemVersion)];
    }
    
    return returnResult;
}



-(NSString *)getJSDDeviceOSVersion
{
    NSString *returnResult;// = [NSString stringWithString:[[UIDevice currentDevice] systemVersion]];
    return returnResult;
}






/************** CORE LOCATION DELEGATE METHODS *************/
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    
    
    CLLocation *currentLocation;
    currentLocation = [locations objectAtIndex:0];
    [_locationManager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (currentLocation != nil) {
             double currentLatitude = [[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] doubleValue];
             double currentLongitude = [[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] doubleValue];
             
             //CLPlacemark *placemark = [placemarks objectAtIndex:0];
             
             _vLatitude = [NSMutableString stringWithFormat:@"%f", currentLatitude];
             _vLongitude = [NSMutableString stringWithFormat:@"%f", currentLongitude];
            
         }
     }];
    
}


-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //_vJSDGeolocation = [NSMutableString stringWithString:@"false"];
}



- (BOOL) appendToFile:(NSURL *)path data:(NSString *)data encoding:(NSStringEncoding)enc
{
    BOOL result = YES;
   
    NSFileHandle* fh = [NSFileHandle fileHandleForReadingFromURL:path error:nil];
    if ( !fh ) {
        [[NSFileManager defaultManager] createFileAtPath:[path path] contents:nil attributes:nil];
        fh = [NSFileHandle fileHandleForWritingAtPath:[path path]];
    }
    else{
        fh = [NSFileHandle fileHandleForWritingAtPath:[path path]];
    }
    @try {
        [fh seekToEndOfFile];
        [fh writeData:[data dataUsingEncoding:enc]];
       
    }
    @catch (NSException * e) {
        result = NO;
    }
    [fh closeFile];
    return result;
}


-(NSString *)getContentFromConfigforKey:(NSString *)itemKey
{
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"conf" ofType:@"plist"];
    NSDictionary *contentDict = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    return [contentDict objectForKey:itemKey];
}

-(BOOL)setContentOnConfigforKey:(NSString *)itemKey value:(NSString *)value
{
    NSFileManager *filemgr = [NSFileManager defaultManager];

    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"conf" ofType:@"plist"];
    if ([filemgr isWritableFileAtPath:plistPath] == YES)
    {
        NSMutableDictionary *contentDict = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
        [contentDict setValue:value forKey:itemKey];
        [contentDict writeToFile:plistPath atomically:YES];

    }
    else
    {

    }
    return YES;
}

- (void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}

@end
