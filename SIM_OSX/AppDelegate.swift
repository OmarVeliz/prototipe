//
//  AppDelegate.swift
//  SIM_OSX
//
//  Created by Fernando Godinez Alvarado on 21/01/16.
//  Copyright © 2016 Plus Mobile Team. All rights reserved.
//

import Cocoa
import ServiceManagement
import TCPIP



@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    var timer:NSTimer!;
    var timerForData:NSTimer!;
    let appInfoClass = FPImport();
    let dataBase = dbManager();
    var userNameVar:String = "";
    var serverIP:String = "";
    var port:String = "";
    var eventMonitor:EventMonitor?;
    let popover = NSPopover()
    var defaults:NSUserDefaults!;

    @IBOutlet weak var window: NSWindow!
    let statusItem = NSStatusBar.systemStatusBar().statusItemWithLength(-2)

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
        //
        
        //appInfoClass.resetDefaults()
        /**************** LAUNCH SERVICE ******************************/
        let launcherAppIdentifier = "com.plusti.SIMOSXLauncherApplication"
        
        var startedAtLogin = false
        for app in NSWorkspace.sharedWorkspace().runningApplications{
            if app.bundleIdentifier == launcherAppIdentifier {
                startedAtLogin = true
            }
        }
        if startedAtLogin{
            NSDistributedNotificationCenter.defaultCenter().postNotificationName("killme", object: NSBundle.mainBundle().bundleIdentifier!)
        }
        
        /**************** LAUNCH SERVICE ******************************/
                //+++++++++++ get info CONFIGTXT +++++++++++++++++++++++++++++
        
        
        
        //---+++++++get MONITORING info ++++++++++++++
        let center:NSDistributedNotificationCenter = NSDistributedNotificationCenter.defaultCenter();
        
        center.addObserver(self, selector: Selector("computerStart:"), name: NSWorkspaceDidWakeNotification, object: nil, suspensionBehavior:(NSNotificationSuspensionBehavior.DeliverImmediately))
        center.addObserver(self, selector: Selector("computerShutdown:"), name: NSWorkspaceWillPowerOffNotification, object: nil, suspensionBehavior:(NSNotificationSuspensionBehavior.DeliverImmediately))
        
        center.addObserver(self, selector: Selector("computerLogin:"), name: "com.apple.sessionDidMoveOnConsole", object: nil, suspensionBehavior:(NSNotificationSuspensionBehavior.DeliverImmediately))
        center.addObserver(self, selector: Selector("computerLogout:"), name: "com.apple.sessionDidMoveOffConsole", object: nil, suspensionBehavior:(NSNotificationSuspensionBehavior.DeliverImmediately))
        
        center.addObserver(self, selector: Selector("screenLocked:"), name: "com.apple.screenIsLocked", object: nil, suspensionBehavior:(NSNotificationSuspensionBehavior.DeliverImmediately))
        center.addObserver(self, selector: Selector("screenUnlocked:"), name: "com.apple.screenIsUnlocked", object: nil, suspensionBehavior:(NSNotificationSuspensionBehavior.DeliverImmediately))
        //---+++++++get monitoring info ++++++++++++++
        
        if let button = statusItem.button {
            button.image = NSImage(named: "StatusBarButtonImage")
            
        }
        
        //+++++++++++ setFileDir +++++++++++++++++++++++++++++
        defaults = NSUserDefaults.standardUserDefaults()
        if (defaults.objectForKey("globalDir") == nil)
        {
            defaults.setObject("", forKey: "globalDir")
        }
        //+++++++++++ setFIleDir +++++++++++++++++++++++++++++
        
        //----++++++ MENU LOGIN INFO +++++++++++++----//
        if let button = statusItem.button {
            button.image = NSImage(named: "StatusBarButtonImage")
            button.action = Selector("togglePopover:")
        }
        
        popover.contentViewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
        
        //----++++++ MENU LOGIN INFO +++++++++++++----//

        
        
        //----++++++ App Event Monitor handler+++++++++++++----//
        eventMonitor = EventMonitor(mask: [NSEventMask.LeftMouseDownMask, NSEventMask.RightMouseDownMask]) { [unowned self] event in
            if self.popover.shown {
                self.closePopover(event)
            }
        }
        eventMonitor?.start()
        //----++++++ App Event Monitor handler+++++++++++++----//
        
        timerForData = NSTimer.scheduledTimerWithTimeInterval(30.0, target: self, selector: "setInfoForApp", userInfo: nil, repeats: true)
        

        //----++++++ Send Data To Monitor +++++++++--------//
        ///enviara la informacion cada 10 minutos
        //timer = NSTimer.scheduledTimerWithTimeInterval(600.0, target: self, selector: "sendDataToMonitor", userInfo: nil, repeats: true)
                //----++++++ Send Data To Monitor +++++++++--------//
        
        
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }

   
    
/************************************************************/
    func setInfoForApp()
    {
        //+++++++++++ get info CONFIGTXT +++++++++++++++++++++++++++++
        let globDir:String = defaults.objectForKey("globalDir") as! String
        if (!globDir.isEmpty)
        {
            dataBase.openDB()
            dataBase.closeDB()
            userNameVar = dataBase.select1FromTable("conf", field:"value", whereString: "key='userName'");
            serverIP = dataBase.select1FromTable("conf", field:"value", whereString: "key='serverIP'");
            port = dataBase.select1FromTable("conf", field:"value", whereString: "key='serverPort'");
            timerForData.invalidate()
            startTimerForSendingData()
        }
        
    }
    func startTimerForSendingData()
    {
        let globDir:String = defaults.objectForKey("globalDir") as! String
        if (!globDir.isEmpty)
        {
            timer = NSTimer.scheduledTimerWithTimeInterval(30.0, target: self, selector: "sendDataToMonitor", userInfo: nil, repeats: true)
        }
    }
    
    func computerStart(notif:NSNotification)
    {
        //print("computer Start!");
        writeInfo(notif.name, trxNumber: "0001")
    
    }
    
    
    func computerShutdown(notif:NSNotification)
    {
        //print("computer Shutdown!");
        writeInfo(notif.name,trxNumber: "0002")
        /*
        https://developer.apple.com/library/mac/documentation/Cocoa/Reference/ApplicationKit/Classes/NSWorkspace_Class/index.html#//apple_ref/c/data/NSWorkspaceSessionDidBecomeActiveNotification
        */
        
        //detect more info
        
    }
   
    func computerLogin(notif:NSNotification)
    {
        //print("Computer login!");
        writeInfo(notif.name,trxNumber: "0003")
        
    }
    func computerLogout(notif:NSNotification)
    {
        //print("computer logoff!");
        writeInfo(notif.name,trxNumber: "0004")
        
    }
    
    func screenLocked(notif:NSNotification)
    {
        //print("Screen is locked!");
        writeInfo(notif.name,trxNumber: "0005")
        
    }
    func screenUnlocked(notif:NSNotification)
    {
        //print("Screen is unlocked!");
        writeInfo(notif.name,trxNumber: "0006")
        
    }
    
    
    func writeInfo(notifName:String,trxNumber:String)
    {
        let globDir:String = defaults.objectForKey("globalDir") as! String
        if (!globDir.isEmpty)
        {
            //let fillStrChar = " "
            
            let date = NSDate()
            
            let calendar = NSCalendar.currentCalendar()
            
            let components = calendar.components([NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second,NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day], fromDate: date)
            
            //print("1")
            
            let datetime = String(format:"%02d%02d%04d%02d%02d",components.day,components.month,components.year,components.hour,components.minute)
            
            let text:NSString = NSString(format:"0%@%@",datetime,trxNumber); //just a text
            print(text)
            if (timer == nil)
            {
                startTimerForSendingData()
            }
            let idString:String = (datetime+trxNumber)
            dataBase.insertIntoTable("log", fieldNames: "id,dataToSend,hasSent", fieldValues: "'\(idString)','\(text)','0'")
        }
    }
    
    
    
    func getIFAddresses() -> [String] {
        var addresses = [String]()
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs> = nil
        if getifaddrs(&ifaddr) == 0 {
            
            // For each interface ...
            for (var ptr = ifaddr; ptr != nil; ptr = ptr.memory.ifa_next) {
                let flags = Int32(ptr.memory.ifa_flags)
                var addr = ptr.memory.ifa_addr.memory
                
                // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
                if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                    if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                        
                        // Convert interface address to a human readable string:
                        var hostname = [CChar](count: Int(NI_MAXHOST), repeatedValue: 0)
                        if (getnameinfo(&addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                            nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                                if let address = String.fromCString(hostname) {
                                    addresses.append(address)
                                }
                        }
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        
        return addresses
    }
    
    
    
    
    func sendDataToMonitor()
    {
        let globDir:String = defaults.objectForKey("globalDir") as! String
        if (!globDir.isEmpty)
        {
            if (Reachability.isConnectedToNetwork() == true)
            {
                do {
                    //print("asdf")
                    let ip = try IP(address: serverIP, port: Int(port)!)
                    let clientSocket = try TCPClientSocket(ip: ip)
            
                    // calls to send append the data to an internal
                    // buffer to minimize system calls
                    let arrayDataToSend:NSMutableArray = dataBase.selectFromTable("log", fields: "dataToSend", whereString:"hasSent='0'")
                    if (arrayDataToSend.count > 0)
                    {
                        for itemIndex in 0...arrayDataToSend.count-1
                        {
                            let sendString:String = String(arrayDataToSend[itemIndex])
                            print("SendingData: ",sendString)
                            try clientSocket.sendString(sendString)
                            // flush actually sends all data in the buffer
                            try clientSocket.flush()
                        }
                        dataBase.updateTable("log", fieldsNamesAndValues:["hasSent":"1"], whereString: "hasSent='0'")
                    }
                } catch {
                    // something bad happened :disappointed:
                }
            }
        }
    }
    
    func showPopover(sender: AnyObject?) {
        if let button = statusItem.button {
            popover.showRelativeToRect(button.bounds, ofView: button, preferredEdge: NSRectEdge.MinY)
        }
        eventMonitor?.start()
    }
    
    internal func closePopover(sender: AnyObject?) {
        popover.performClose(sender)
        
    }
    
    func togglePopover(sender: AnyObject?) {
        if popover.shown {
            closePopover(sender)
        } else {
            showPopover(sender)
        }
        eventMonitor?.stop()
    }
    
}
