//
//  FPDiOS.h
//  FPDiOS
//
//  Created by Fernando Godinez Alvarado on 7/10/15.
//  Copyright © 2015 Plus Mobile Team. All rights reserved.
//


#import <AppKit/AppKit.h>


@import CoreLocation;
@import CoreTelephony;
@import LocalAuthentication;
//! Project version number for FPDiOS.
FOUNDATION_EXPORT double FPDiOSVersionNumber;

//! Project version string for FPDiOS.
FOUNDATION_EXPORT const unsigned char FPDiOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FPDiOS/PublicHeader.h>

//@interface FPDiOS : UIDevice, CLLocationManagerDelegate
@interface FPImport : NSObject <CLLocationManagerDelegate>


@property (nonatomic,strong) CLLocationManager *locationManager;

/*-------------------*/
@property (nonatomic,strong) NSMutableString *vtransactionUUID;
@property (nonatomic,strong) NSMutableString *vtransactionType;
@property (nonatomic,strong) NSMutableString *vUsername;
@property (nonatomic,strong) NSMutableString *vClientDateStamp;
@property (nonatomic,strong) NSMutableString *vClientTimeStamp;
@property (nonatomic,strong) NSMutableString *vLatitude;
@property (nonatomic,strong) NSMutableString *vLongitude;
@property (nonatomic,strong) NSMutableString *vHostName;
@property (nonatomic,strong) NSMutableString *vMACAddress;
@property (nonatomic,strong) NSMutableString *vIPPrivAddress;
@property (nonatomic,strong) NSMutableString *vIPPublAddress;
@property (nonatomic,strong) NSMutableString *vIPHostISP;
@property (nonatomic,strong) NSMutableString *vDeviceBrand;
@property (nonatomic,strong) NSMutableString *vDeviceModel;
@property (nonatomic,strong) NSMutableString *vDeviceLanguage;
@property (nonatomic,strong) NSMutableString *vDeviceOS;
@property (nonatomic,strong) NSMutableString *vUserPass;

-(NSString *)getFPD;
- (BOOL) appendToFile:(NSURL *)path data:(NSString *)data encoding:(NSStringEncoding)enc;
-(NSString *)getContentFromConfigforKey:(NSString *)itemKey;
-(BOOL)setContentOnConfigforKey:(NSString *)itemKey value:(NSString *)value;
-(NSString *)getStringFillwith:(NSString *)str size:(int)size withFillChar:(NSString *)fillStr;
-(NSString *)getHostName;
-(NSString *)getPublicIP;
-(NSString *)getMacAddress;
-(NSString *)getMacIPAddress;
-(NSString *)getDeviceBrand;
-(NSString *)getMacDeviceModel;
-(NSString *)getMacLanguage;
-(NSString *)getMacDeviceOS;
-(void)resetDefaults;

//-(NSMutableDictionary *)obtainConfPList;

@end
