//
//  dbManager.swift
//  SIM_OSX
//
//  Created by Fernando Godinez Alvarado on 25/02/16.
//  Copyright © 2016 Plus Mobile Team. All rights reserved.
//
// fuente: http://stackoverflow.com/questions/24102775/accessing-an-sqlite-database-in-swift
//

import Foundation

import Cocoa
public class dbManager
{
    private var dataBaseURL: NSURL
    private var db: COpaquePointer
    private var statement: COpaquePointer
    public var userSelectedDirectory:NSURL
    internal let defaults:NSUserDefaults!
    
    internal let SQLITE_STATIC = unsafeBitCast(0, sqlite3_destructor_type.self)
    internal let SQLITE_TRANSIENT = unsafeBitCast(-1, sqlite3_destructor_type.self)
    
    public init() {
        self.dataBaseURL = NSURL.init()
        self.db = nil
        self.statement = nil
        self.userSelectedDirectory = NSURL.init()
        self.defaults = NSUserDefaults.standardUserDefaults()
    }
    
    
    func openDB() -> Bool
    {
        let globDir:String = self.defaults.objectForKey("globalDir") as! String
        print("address: ",globDir)
        if (!globDir.isEmpty)
        {
            let documents = NSURL.fileURLWithPath(globDir)
            let dataBaseURLDBDoc:NSURL = documents.URLByAppendingPathComponent("SIM_Data.sqlite3")
            sqlite3_extended_result_codes(self.db,1)
                print("address with file: ",dataBaseURLDBDoc.path)
            if sqlite3_open_v2(dataBaseURLDBDoc.path!, &self.db, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE, nil)  != SQLITE_OK {
                print(sqlite3_errmsg(self.db))
                print(sqlite3_extended_errcode(self.db))
                
                
                print("error opening database")
                return false;
            }
            else
            {
                
                SQLITE_FCNTL_PRAGMA/*this line supposes to prevent the using of WAL*/
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    
    func createIfNotExistsTable(tableName:String,fieldsString:String)
    {
        
        if (openDB())
        {
            //fields example: "id integer primary key autoincrement, hasSent text, dataToSend text"
            if sqlite3_exec(self.db, "create table if not exists " + tableName + " (" + fieldsString + ")", nil, nil, nil) != SQLITE_OK {
                let errmsg = String.fromCString(sqlite3_errmsg(db))
                print("error creating table: \(errmsg)")
            }
            closeDB()
        }
    }
    
    func insertIntoTable(tableName:String,fieldNames:String,fieldValues:String)
    {
        statement = nil
        if (openDB())
        {
            if sqlite3_prepare_v2(db, "insert into " + tableName + " (" + fieldNames + ") values (" + fieldValues + ")", -1, &statement, nil) != SQLITE_OK {
                let errmsg = String.fromCString(sqlite3_errmsg(db))
                print("error preparing insert: \(errmsg)")
            }
            
            //values example: "fernando, godinez"
            /*if sqlite3_bind_text(statement, 1, fieldValues, -1, SQLITE_TRANSIENT) != SQLITE_OK {
                let errmsg = String.fromCString(sqlite3_errmsg(db))
                print("failure binding insert values: \(errmsg)")
            }*/
            
            if sqlite3_step(statement) != SQLITE_DONE {
                let errmsg = String.fromCString(sqlite3_errmsg(db))
                print("failure inserting on database: \(errmsg)")
            }
            
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String.fromCString(sqlite3_errmsg(db))
                print("error finalizing prepared insert statement: \(errmsg)")
            }
            
            statement = nil
            closeDB()
        }
    }
    
    
    func updateTable(tableName:String,fieldsNamesAndValues:NSMutableDictionary,whereString:String)
    {
        if (openDB())
        {
            statement = nil
            var querySet:String = ""
            
            for (key,value) in fieldsNamesAndValues{
                querySet += "\(key)='\(value)', "
            }
            querySet = String(querySet.characters.dropLast(2))
            print("update " + tableName + " set " + querySet + " where " + whereString)
            if sqlite3_prepare_v2(db, "update " + tableName + " set " + querySet + " where " + whereString, -1, &statement, nil) != SQLITE_OK {
                let errmsg = String.fromCString(sqlite3_errmsg(db))
                print("error preparing update: \(errmsg)")
            }
            
            //values example: "fernando, godinez"
            /*if sqlite3_bind_text(statement, 1, whereString, -1, SQLITE_TRANSIENT) != SQLITE_OK {
                let errmsg = String.fromCString(sqlite3_errmsg(db))
                print("failure binding Where for update: \(errmsg)")
            }*/
            
            if sqlite3_step(statement) != SQLITE_DONE {
                let errmsg = String.fromCString(sqlite3_errmsg(db))
                print("failure updating on database: \(errmsg)")
            }
            
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String.fromCString(sqlite3_errmsg(db))
                print("error finalizing prepared update statement: \(errmsg)")
            }
            
            statement = nil
            closeDB()
        }
    }

    
    func selectFromTable(tableName:String,fields:String,whereString:String) -> NSMutableArray
    {
        if (openDB())
        {
            let returnResult:NSMutableArray = NSMutableArray()
            statement = nil
            if (whereString.isEmpty)
            {
                if sqlite3_prepare_v2(db, "select " + fields + " from " + tableName, -1, &statement, nil) != SQLITE_OK {
                    let errmsg = String.fromCString(sqlite3_errmsg(db))
                    print("error preparing select: \(errmsg)")
                }
            }
            else //has where
            {
                if sqlite3_prepare_v2(db, "select " + fields + " from " + tableName + " where " + whereString, -1, &statement, nil) != SQLITE_OK {
                    let errmsg = String.fromCString(sqlite3_errmsg(db))
                    print("error preparing select: \(errmsg)")
                }
            }
            
            while sqlite3_step(statement) == SQLITE_ROW {
                let rowArray:NSMutableArray = NSMutableArray()
                
                let columnCount = sqlite3_column_count(statement)
                for colIndex in 0...columnCount-1
                {
                    let columnText = sqlite3_column_text(statement, colIndex)
                    if columnText != nil {
                        rowArray.addObject(String.fromCString(UnsafePointer<Int8>(columnText))!)
                    }
                }
                returnResult.addObject(rowArray)
                
            }
            
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String.fromCString(sqlite3_errmsg(db))
                print("error finalizing prepared forSelect: \(errmsg)")
            }
            
            statement = nil
            closeDB()
            return returnResult
        }
        else
        {
            return [""]
        }
    }
    
    
    func closeDB()
    {
        if sqlite3_close(db) != SQLITE_OK {
            print("error closing database")
        }
        //sqlite3_finalize(db)
        db = nil
    }
    
    func select1FromTable(tableName:String,field:String,whereString:String) -> String
    {
        if (openDB())
        {
            var returnResult:String = ""
            statement = nil
            if (whereString.isEmpty)
            {
                if sqlite3_prepare_v2(db, "select " + field + " from " + tableName, -1, &statement, nil) != SQLITE_OK {
                    let errmsg = String.fromCString(sqlite3_errmsg(db))
                    print("error preparing select: \(errmsg)")
                }
            }
            else //has where
            {
                
                if sqlite3_prepare_v2(db, "select " + field + " from " + tableName + " where " + whereString, -1, &statement, nil) != SQLITE_OK {
                    let errmsg = String.fromCString(sqlite3_errmsg(db))
                    print("error preparing select where: \(errmsg)")
                }
            }
            
            while sqlite3_step(statement) == SQLITE_ROW {
                let columnText = sqlite3_column_text(statement, 0)
                if columnText != nil {
                    returnResult = String.fromCString(UnsafePointer<Int8>(columnText))!
                }
            }
            
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String.fromCString(sqlite3_errmsg(db))
                print("error finalizing prepared forSelect: \(errmsg)")
            }
            
            statement = nil
            closeDB()
            return returnResult
        }
        else
        {
            return ""
        }
    }
    
    func selectIfExistsConfTable() -> Bool
    {
        if (openDB())
        {
            print("count: ",select1FromTable("conf", field: "count(1)", whereString: ""))
            if ((select1FromTable("conf", field: "count(1)", whereString: "") == "0") || (select1FromTable("conf", field: "count(1)", whereString: "") == "6"))
            {
                closeDB()
                return true;
            }
            else
            {
                closeDB()
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
}