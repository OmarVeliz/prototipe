//
//  LoginViewController.swift
//  SIM_OSX
//
//  Created by Fernando Godinez Alvarado on 23/02/16.
//  Copyright © 2016 Plus Mobile Team. All rights reserved.
//

import Cocoa
import CryptoSwift


public class LoginViewController: NSViewController {

    let alert:NSAlert = NSAlert();
    let nameAlert:NSAlert = NSAlert();
    let dataBase = dbManager();
    let defaults = NSUserDefaults.standardUserDefaults()

    
    //var dataBaseDir:NSURL = NSURL();
    
    @IBOutlet var dirButton: NSButton!
    @IBOutlet var dismissText: NSTextField!
    @IBOutlet var descriptionText: NSTextField!
    @IBOutlet var approvalImage: NSButton!
    @IBOutlet var backgroundImage: NSButtonCell!
    //@IBOutlet var titleTextView: NSTextField!
    @IBOutlet var passTextView: NSTextField!
    @IBOutlet var userTextView: NSTextField!
    @IBOutlet var signmeButton: NSButton!
    @IBOutlet var resultTextView: NSTextField!
    
    let appInfoClass = FPImport();
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        let globDir:String = defaults.objectForKey("globalDir") as! String
        if (globDir.isEmpty)
        {
            //let userNameVar = dataBase.select1FromTable("conf", field:"value", whereString: "key='userName'");
        //if (userNameVar.isEmpty)
        //{//apertura inicial - no usuario registrado
            resultTextView.hidden = true
            resultTextView.stringValue = "Welcome!"
            approvalImage.hidden = true
            descriptionText.stringValue = "We've backed up your info!! \n It's safe to continue..."
            descriptionText.backgroundColor = NSColor.whiteColor()
            descriptionText.hidden = true
        
            
            
            dismissText.stringValue = "Click on any place outside this view, to dismiss."
            dismissText.backgroundColor = NSColor.whiteColor()
            dismissText.hidden = true
        
            let pstyle = NSMutableParagraphStyle()
            pstyle.alignment = .Center
            signmeButton.attributedTitle = NSAttributedString(string: "Log Me", attributes: [ NSForegroundColorAttributeName : NSColor.whiteColor(), NSParagraphStyleAttributeName : pstyle ])
            
            dirButton.hidden = false
            dirButton.attributedTitle = NSAttributedString(string: "Choose Path", attributes: [ NSForegroundColorAttributeName : NSColor.whiteColor(), NSParagraphStyleAttributeName : pstyle ])
        }
        else
        {//aperturas siguientes
            resultTextView.hidden = false
            let pstyle = NSMutableParagraphStyle()
            pstyle.alignment = .Center
            
            signmeButton.attributedTitle = NSAttributedString(string: "Ok", attributes: [ NSForegroundColorAttributeName : NSColor.whiteColor(), NSParagraphStyleAttributeName : pstyle ])
            signmeButton.hidden = true
            
            passTextView.hidden = true
            userTextView.hidden = true
            backgroundImage.image = NSImage(named: "background_welcome")
            approvalImage.hidden = false
            descriptionText.hidden = false
            dismissText.hidden = false
            
            resultTextView.hidden = false
            resultTextView.stringValue = "Welcome!"
            approvalImage.hidden = false
            descriptionText.stringValue = "We've backed up your info!! \n It's safe to continue..."
            descriptionText.backgroundColor = NSColor.whiteColor()
            descriptionText.hidden = false
            
            dirButton.hidden = true
            
            dismissText.stringValue = "Click on any place outside this view, to dismiss."
            dismissText.backgroundColor = NSColor.whiteColor()
            dismissText.hidden = false
        }
        
    }
    
    @IBAction func saveUser(sender: AnyObject) {
        if (signmeButton.title == "Ok")
        {
            //let appdel:AppDelegate
            // AppDelegate.closePopover(appdel)
            //NSApplication.sharedApplication().delegate?.self.performSelector("closePopover:")
            
            // save the presenting ViewController
            
        }
        else
        {
            let defaults = NSUserDefaults.standardUserDefaults()
            let dbdir:String = defaults.objectForKey("globalDir") as! String
            if ((dbdir.isEmpty) || (userTextView.stringValue.isEmpty) || (passTextView.stringValue.isEmpty))
            {
                nameAlert.messageText = "Please, enter your Username, Password and Choose directory for Saving your App Private Info."
                nameAlert.informativeText = "SIM"
                nameAlert.runModal()
            }
            else
            {
                var valuesToUpdate:NSMutableDictionary = ["value":userTextView.stringValue];
                
                dataBase.updateTable("conf", fieldsNamesAndValues: valuesToUpdate, whereString: "key='userName'")
                valuesToUpdate=["value":passTextView.stringValue.sha256()];
                dataBase.updateTable("conf", fieldsNamesAndValues: valuesToUpdate, whereString: "key='password'")
                
                //guardaEnConf()
                print(dataBase.select1FromTable("conf", field:"value", whereString: "key='userName'"))
                print(dataBase.select1FromTable("conf", field:"value", whereString: "key='password'"))
                //loadGameData()
                resultTextView.hidden = false
                let pstyle = NSMutableParagraphStyle()
                pstyle.alignment = .Center
                
                signmeButton.attributedTitle = NSAttributedString(string: "Ok", attributes: [ NSForegroundColorAttributeName : NSColor.whiteColor(), NSParagraphStyleAttributeName : pstyle ])
                signmeButton.hidden = true
                
                dirButton.hidden = true
                
                passTextView.hidden = true
                userTextView.hidden = true
                backgroundImage.image = NSImage(named: "background_welcome")
                approvalImage.hidden = false
                descriptionText.hidden = false
                dismissText.hidden = false
            }
        }
        
    }
    
    @IBAction func AddUserPath(sender: AnyObject) {
        let openPanel = NSOpenPanel();
        openPanel.allowsMultipleSelection = false;
        openPanel.canChooseDirectories = true;
        openPanel.canCreateDirectories = true;
        openPanel.canChooseFiles = false;
        
        let globDir:String = defaults.objectForKey("globalDir") as! String
        if (globDir.isEmpty)
        {
            let i = openPanel.runModal();
            if(i == NSModalResponseOK){
               
                dataBase.userSelectedDirectory = openPanel.URL!
                defaults.setObject(openPanel.URL?.absoluteString, forKey: "globalDir")
                dataBase.openDB()
                dataBase.closeDB()
                dataBase.createIfNotExistsTable("conf", fieldsString: "key text primary key, value text")
                dataBase.insertIntoTable("conf", fieldNames: "key,value", fieldValues: "'serverIP','192.168.120.128'")
                dataBase.insertIntoTable("conf", fieldNames: "key,value", fieldValues: "'serverPort','5555'")
                dataBase.insertIntoTable("conf", fieldNames: "key,value", fieldValues: "'userName',''")
                dataBase.insertIntoTable("conf", fieldNames: "key,value", fieldValues: "'password',''")
                dataBase.insertIntoTable("conf", fieldNames: "key,value", fieldValues: "'sentItems','0'")
                dataBase.insertIntoTable("conf", fieldNames: "key,value", fieldValues: "'failedItems','0'")
                dataBase.createIfNotExistsTable("log", fieldsString: "id text primary key, hasSent text, dataToSend text")
                
            }
        }
        else
        {
            dataBase.userSelectedDirectory = NSURL(fileURLWithPath:defaults.objectForKey("globalDir") as! String)
            

        }

    }
}
